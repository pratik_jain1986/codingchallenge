﻿// <copyright file="AddressFetchException.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Exceptions
{
	using System;
	public class AddressFetchException : Exception
	{
		public string GoogleErrorStatusCode;

		public AddressFetchException(string message, string status)
			: base(message: message)
		{
			GoogleErrorStatusCode = status;
		}
	}
}
