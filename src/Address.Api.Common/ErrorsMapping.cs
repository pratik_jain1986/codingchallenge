﻿// <copyright file="ErrorsMapping.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common
{
    using System.Collections.Generic;
	using System.Net;
	using Address.Api.Common.Constants;

	/// <summary>
	/// Class ErrorsMapping.
	/// </summary>
	public static class ErrorsMapping
	{
		/// <summary>
		/// The validation error codes
		/// </summary>
		public static readonly Dictionary<string, HttpStatusCode> ValidationErrorCodes = new()
		{
			{ ErrorCodes.BadRequest, HttpStatusCode.BadRequest },
			{ ErrorCodes.UnknownError, HttpStatusCode.InternalServerError }
		};
	}
}
