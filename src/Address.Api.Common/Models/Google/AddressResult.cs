﻿// <copyright file="AddressResult.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Models.Google
{
	using Newtonsoft.Json;

	/// <summary>
	/// Class AddressResult.
	/// </summary>
	public class AddressResult
	{
		/// <summary>
		/// Gets or sets the formatted address.
		/// </summary>
		/// <value>The formatted address.</value>
		[JsonProperty(PropertyName = "formatted_address", NullValueHandling = NullValueHandling.Ignore)]
		public string FormattedAddress { get; set; }

		/// <summary>
		/// Gets or sets the place identifier.
		/// </summary>
		/// <value>The place identifier.</value>
		[JsonProperty(PropertyName = "place_id", NullValueHandling = NullValueHandling.Ignore)]
		public string PlaceId { get; set; }
	}
}
