﻿// <copyright file="ResponseAddress.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Models.Google
{
	using Newtonsoft.Json;

	/// <summary>
	/// Class ResponseAddress.
	/// </summary>
	public class ResponseAddress
	{
		/// <summary>
		/// Gets or sets the address results.
		/// </summary>
		/// <value>The address results.</value>
		[JsonProperty(PropertyName = "results", NullValueHandling = NullValueHandling.Ignore)]
		public AddressResult[] AddressResults { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		/// <value>The status.</value>
		[JsonProperty(PropertyName = "status")]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>The error message.</value>
		[JsonProperty(PropertyName = "error_message", NullValueHandling = NullValueHandling.Ignore)]
		public string ErrorMessage { get; set; }
	}
}
