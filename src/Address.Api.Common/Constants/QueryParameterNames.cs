﻿// <copyright file="QueryParameterNames.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Constants
{
	/// <summary>
	/// Class QueryParameterNames.
	/// </summary>
	public class QueryParameterNames
	{
		/// <summary>
		/// The destination location lat long
		/// </summary>
		public const string DestinationLocationLatLong = "destinationLocationLatLong";

		/// <summary>
		/// The pickup location lat long
		/// </summary>
		public const string PickupLocationLatLong = "pickupLocationLatLong";
	}
}
