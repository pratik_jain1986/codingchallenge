﻿// <copyright file="ErrorMessages.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Constants
{
	/// <summary>
	/// Class ErrorMessages.
	/// </summary>
	public static class ErrorMessages
	{
		/// <summary>
		/// The get address failed
		/// </summary>
		public const string GetAddressFailed = "Get address failed";

		/// <summary>
		/// The invalid input
		/// </summary>
		public const string InvalidInput = "Input request is null";

		/// <summary>
		/// The invalid input location
		/// </summary>
		public const string InvalidInputLocation = "Input location is invalid";

		/// <summary>
		/// The invalid lat long
		/// </summary>
		public const string InvalidLatLong = "Invalid lat long value";

		/// <summary>
		/// The failed to fetch address
		/// </summary>
		public const string FailedToFetchAddress = "Failed to fetch address";
	}
}
