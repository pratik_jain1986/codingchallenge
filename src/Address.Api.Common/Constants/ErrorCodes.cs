﻿
namespace Address.Api.Common.Constants
{
	/// <summary>
	/// Class ErrorCodes.
	/// </summary>
	public static class ErrorCodes
	{
		/// <summary>
		/// The unknown error
		/// </summary>
		public const string UnknownError = "address-001";

		/// <summary>
		/// The bad request
		/// </summary>
		public const string BadRequest = "address-002";

		/// <summary>
		/// The not acceptable
		/// </summary>
		public const string NotAcceptable = "address-003";
	}
}
