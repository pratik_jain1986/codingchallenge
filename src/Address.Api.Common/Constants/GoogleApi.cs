﻿// <copyright file="GoogleApi.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Common.Constants
{
	/// <summary>
	/// Class GoogleApi.
	/// </summary>
	public static class GoogleApi
	{
		/// <summary>
		/// The base address
		/// </summary>
		public const string BaseAddress = "https://maps.googleapis.com/maps/api/";

		/// <summary>
		/// The geo code path template
		/// </summary>
		public const string GeoCodePathTemplate = "geocode/json?latlng={0}&key={1}";

		/// <summary>
		/// The HTTP client name
		/// </summary>
		public const string HttpClientName = "Google";

		/// <summary>
		/// The status ok
		/// </summary>
		public const string StatusOk = "OK";
	}
}
