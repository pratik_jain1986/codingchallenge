﻿// <copyright file="GoogleApiConfiguration.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Models
{
    using Contracts;

    /// <summary>
    /// Class GoogleApiConfiguration.
    /// </summary>
    public class GoogleApiConfiguration : IGoogleApiConfiguration
    {
        /// <summary>
        /// Gets or sets the API key.
        /// </summary>
        /// <value>The API key.</value>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the base address.
        /// </summary>
        /// <value>The base address.</value>
        public string BaseAddress { get; set; }
    }
}
