﻿namespace Address.Api.Models.Request
{
    /// <summary>
    /// Class Address Request
    /// </summary>
    public class AddressRequest
    {
        public AddressRequest(string pickupLocationLatLong, string destinationLocationLatLong) {
            PickupLocationLatLong = pickupLocationLatLong;
            DestinationLocationLatLong = destinationLocationLatLong;
        }

        /// <summary>
        /// Gets or sets pickup location lat long
        /// </summary>
        public string PickupLocationLatLong { get;}

        /// <summary>
        /// Get or set destination location lat long
        /// </summary>
        public string DestinationLocationLatLong { get; }
    }
}
