﻿// <copyright file="Address.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

using Newtonsoft.Json;

namespace Address.Api.Models
{
    /// <summary>
    /// Class Address
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Gets or sets formatted address.
        /// </summary>
        /// <value>The formatted address.</value>
        [JsonProperty(PropertyName = "address")]
        public string FormattedAddress { get; set; }


        /// <summary>
        /// Gets or sets the place identifier.
        /// </summary>
        /// <value>The place identifier.</value>
        [JsonProperty(PropertyName = "placeId")]
        public string PlaceId { get; set; }

    }
}
