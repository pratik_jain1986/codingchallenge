﻿// <copyright file="AddressResponse.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Models.Response
{
	using System.Collections.Generic;
	using Newtonsoft.Json;

	/// <summary>
	/// Class AddressResponse.
	/// </summary>
	public class AddressResponse
	{
		/// <summary>
		/// Gets or sets the pickup locations.
		/// </summary>
		/// <value>The pickup locations.</value>
		[JsonProperty(PropertyName = "pickupLocations")]
		public List<Address> PickupLocations { get; set; }

		/// <summary>
		/// Gets or sets the destination.
		/// </summary>
		/// <value>The destination.</value>
		[JsonProperty(PropertyName = "destinationLocations")]
		public List<Address> DestinationLocations { get; set; }
	}
}
