﻿// <copyright file="ErrorResponse.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Models.Response
{
	/// <summary>
	/// Class ErrorResponse.
	/// </summary>
	public class ErrorResponse
	{
		/// <summary>
		/// Gets or sets the error code.
		/// </summary>
		/// <value>The error code.</value>
		public string ErrorCode { get; set; }

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>The error message.</value>
		public string ErrorMessage { get; set; }
	}
}
