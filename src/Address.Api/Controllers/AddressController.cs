﻿// <copyright file="AddressController.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Controllers
{
	using System;
	using System.Linq;
	using System.Net;
	using System.Threading.Tasks;
	using Address.Api.Common;
	using Address.Api.Common.Constants;
	using Address.Api.Common.Exceptions;
	using Address.Api.Contracts;
	using Address.Api.Extensions;
	using Address.Api.Models.Request;
	using Address.Api.Models.Response;
	using FluentValidation;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.Logging;

	/// <summary>
	/// Class AddressController.
	/// Implements the <see cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
	/// </summary>
	/// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
	[ApiController]
	[Route(template: "[controller]")]
	public class AddressController : ControllerBase
	{
		/// <summary>
		/// The address helper
		/// </summary>
		private readonly IAddressHelper _addressHelper;
		/// <summary>
		/// The logger
		/// </summary>
		private readonly ILogger<AddressController> _logger;

		/// <summary>
		/// Initializes a new instance of the <see cref="AddressController"/> class.
		/// </summary>
		/// <param name="logger">The logger.</param>
		/// <param name="addressHelper">The address helper.</param>
		public AddressController(ILogger<AddressController> logger, IAddressHelper addressHelper)
		{
			_logger = logger;
			_addressHelper = addressHelper;
		}

		/// <summary>
		/// Gets the specified pickup location lat long.
		/// </summary>
		/// <param name="pickupLocationLatLong">The pickup location lat long.</param>
		/// <param name="destinationLocationLatLong">The destination location lat long.</param>
		/// <returns>Task&lt;IActionResult&gt;.</returns>
		[HttpGet]
		public async Task<IActionResult> GetAddress(
			[FromQuery(Name = QueryParameterNames.PickupLocationLatLong)] string pickupLocationLatLong,
			[FromQuery(Name = QueryParameterNames.DestinationLocationLatLong)] string destinationLocationLatLong)
		{
			try
			{
				var addressRequest = new AddressRequest(pickupLocationLatLong: pickupLocationLatLong,
																								destinationLocationLatLong: destinationLocationLatLong);

				var response = await _addressHelper.GetAddress(addressRequest: addressRequest);

				return StatusCode(statusCode: (int)HttpStatusCode.OK, value: response);
			}
			catch (ValidationException validationException)
			{
				var error = validationException.Errors.First();

				var errorResponse = GetErrorResponse(errorCode: error.ErrorCode, errorMessage: error.ErrorMessage);

				var statusCode = validationException.ToStatusCode(errorCodesMapping: ErrorsMapping.ValidationErrorCodes);

				return StatusCode(statusCode: (int)statusCode, value: errorResponse);
			}
			catch (AddressFetchException addressFetchException)
			{
				var errorResponse = GetErrorResponse(errorCode: addressFetchException.GoogleErrorStatusCode,
																						errorMessage: addressFetchException.Message);

				return StatusCode(statusCode: (int)HttpStatusCode.NotAcceptable, value: errorResponse);
			}
			catch (Exception exception)
			{
				_logger.LogCritical(message: $"Failed to create score due to {exception.Message}");

				var errorResponse =
					GetErrorResponse(errorCode: ErrorCodes.UnknownError, errorMessage: ErrorMessages.GetAddressFailed);

				return StatusCode(statusCode: (int)HttpStatusCode.InternalServerError, value: errorResponse);
			}
		}

		/// <summary>
		/// Gets the error response.
		/// </summary>
		/// <param name="errorCode">The error code.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <returns>ErrorResponse.</returns>
		private ErrorResponse GetErrorResponse(string errorCode, string errorMessage)
		{
			return new()
			{
				ErrorCode = errorCode,
				ErrorMessage = errorMessage
			};
		}
	}
}
