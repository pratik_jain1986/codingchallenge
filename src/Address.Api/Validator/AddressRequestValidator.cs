﻿// <copyright file="AddressRequestValidator.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Validators
{
	using System.Collections.Generic;
	using Address.Api.Common.Constants;
	using Address.Api.Contracts;
	using Address.Api.Models.Request;
	using FluentValidation;
	using FluentValidation.Results;

	/// <summary>
	/// Class AddressRequestValidator.
	/// Implements the <see cref="FluentValidation.AbstractValidator{Address.Api.Models.Request.AddressRequest}" />
	/// Implements the <see cref="Address.Api.Contracts.IAddressRequestValidator" />
	/// </summary>
	public class AddressRequestValidator : AbstractValidator<AddressRequest>, IAddressRequestValidator
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AddressRequestValidator"/> class.
		/// </summary>
		public AddressRequestValidator()
		{
			RuleFor(expression: request => request)
				.Cascade(cascadeMode: CascadeMode.StopOnFirstFailure)
				.Must(predicate: r => HasValidInput(value: r.PickupLocationLatLong))
				.WithMessage(errorMessage: ErrorMessages.InvalidInputLocation)
				.WithErrorCode(errorCode: ErrorCodes.BadRequest)
				.Must(predicate: r => HasValidInput(value: r.DestinationLocationLatLong))
				.WithMessage(errorMessage: ErrorMessages.InvalidInputLocation)
				.WithErrorCode(errorCode: ErrorCodes.BadRequest)
				.Must(predicate: r => HasValidLatLong(locationLatLong: r.PickupLocationLatLong))
				.WithMessage(errorMessage: ErrorMessages.InvalidLatLong)
				.WithErrorCode(errorCode: ErrorCodes.BadRequest)
				.Must(predicate: r => HasValidLatLong(locationLatLong: r.DestinationLocationLatLong))
				.WithMessage(errorMessage: ErrorMessages.InvalidLatLong)
				.WithErrorCode(errorCode: ErrorCodes.BadRequest);
		}

		public override ValidationResult Validate(ValidationContext<AddressRequest> context)
		{
			if (context.InstanceToValidate == null)
			{
				return new ValidationResult(failures: new List<ValidationFailure>
				{
					new(propertyName: nameof(context), errorMessage: ErrorMessages.InvalidInput)
					{
						ErrorCode = ErrorCodes.BadRequest
					}
				});
			}

			return base.Validate(context: context);
		}

		/// <summary>
		/// Determines whether [has valid input] [the specified value].
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>System.Boolean.</returns>
		private bool HasValidInput(string value)
		{
			return !string.IsNullOrWhiteSpace(value: value);
		}

		/// <summary>
		/// Determines whether [has valid lat long] [the specified location lat long].
		/// </summary>
		/// <param name="locationLatLong">The location lat long.</param>
		/// <returns>System.Boolean.</returns>
		private bool HasValidLatLong(string locationLatLong)
		{
			var latLong = locationLatLong.Split(separator: ",");

			if (latLong.Length != 2)
			{
				return false;
			}

			if (double.TryParse(s: latLong[0], result: out var latitude))
			{
				if (!IsValidLatValue(latitude: latitude))
				{
					return false;
				}
			}

			if (double.TryParse(s: latLong[1], result: out var longitude))
			{
				if (!IsValidLongValue(longitude: latitude))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Determines whether [is valid lat value] [the specified latitude].
		/// </summary>
		/// <param name="latitude">The latitude.</param>
		/// <returns>System.Boolean.</returns>
		private bool IsValidLatValue(double latitude)
		{
			return latitude is >= -90 and <= 90;
		}

		/// <summary>
		/// Determines whether [is valid long value] [the specified longitude].
		/// </summary>
		/// <param name="longitude">The longitude.</param>
		/// <returns>System.Boolean.</returns>
		private bool IsValidLongValue(double longitude)
		{
			return longitude is >= -180 and <= 180;
		}
	}
}
