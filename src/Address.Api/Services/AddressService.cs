﻿// <copyright file="AddressService.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Services
{
	using System;
	using System.Net.Http;
	using System.Threading.Tasks;
	using Address.Api.Common.Constants;
	using Address.Api.Common.Exceptions;
	using Address.Api.Common.Models.Google;
	using Address.Api.Contracts;
	using Newtonsoft.Json;

	/// <summary>
	/// Class AddressService.
	/// Implements the <see cref="IAddressService" />
	/// </summary>
	public class AddressService : IAddressService
	{
		private readonly IGoogleApiConfiguration _googleApiConfiguration;

		/// <summary>
		/// The HTTP client
		/// </summary>
		private readonly HttpClient _httpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="AddressService" /> class.
		/// </summary>
		/// <param name="clientFactory">The client factory.</param>
		/// <param name="googleApiConfiguration">The google API configuration.</param>
		public AddressService(IHttpClientFactory clientFactory, IGoogleApiConfiguration googleApiConfiguration)
		{
			_googleApiConfiguration = googleApiConfiguration;
			_httpClient = clientFactory.CreateClient(name: GoogleApi.HttpClientName);
		}

		/// <inheritdoc />
		public async Task<ResponseAddress> GetAddress(string latLong)
		{
			try
			{
				var geoCodeRequestPath =
					string.Format(format: GoogleApi.GeoCodePathTemplate, arg0: latLong, arg1: _googleApiConfiguration.ApiKey);

				var geoCodeRequest = new HttpRequestMessage(method: HttpMethod.Get, requestUri: geoCodeRequestPath);

				var response = await _httpClient.SendAsync(request: geoCodeRequest);

				if (response.IsSuccessStatusCode)
				{
					var responseAsJson = await response.Content.ReadAsStringAsync();

					var responseAddress = JsonConvert.DeserializeObject<ResponseAddress>(value: responseAsJson);

					// Return response only if the status ok
					if (responseAddress.Status == GoogleApi.StatusOk)
					{
						return responseAddress;
					}
				}

				throw new Exception(message: ErrorMessages.FailedToFetchAddress);
			}
			catch (Exception ex)
			{
				throw new AddressFetchException(message: ex.Message, status: ErrorCodes.NotAcceptable);
			}
		}
	}
}
