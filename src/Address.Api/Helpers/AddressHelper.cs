﻿// <copyright file="AddressHelper.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Helpers
{
	using System.Threading.Tasks;
	using Address.Api.Contracts;
	using Address.Api.Extensions;
	using Address.Api.Models.Request;
	using Address.Api.Models.Response;
	using FluentValidation;

	public class AddressHelper : IAddressHelper
	{
		private readonly IAddressRequestValidator _addressRequestValidator;
		private readonly IAddressService _addressService;

		public AddressHelper(IAddressService addressService, IAddressRequestValidator addressRequestValidator)
		{
			_addressService = addressService;
			_addressRequestValidator = addressRequestValidator;
		}

		/// <inheritdoc />
		public async Task<AddressResponse> GetAddress(AddressRequest addressRequest)
		{
			_addressRequestValidator.ValidateAndThrow(instance: addressRequest);

			var pickupLocationAddressResponse =
				await _addressService.GetAddress(latLong: addressRequest.PickupLocationLatLong);

			var destinationLocationAddressResponse =
				await _addressService.GetAddress(latLong: addressRequest.DestinationLocationLatLong);

			return new AddressResponse
			{
				PickupLocations = pickupLocationAddressResponse.ToAddresses(),
				DestinationLocations = destinationLocationAddressResponse.ToAddresses()
			};
		}
	}
}
