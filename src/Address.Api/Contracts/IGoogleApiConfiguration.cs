﻿// <copyright file="IGoogleApiConfiguration.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Contracts
{
	public interface IGoogleApiConfiguration
	{
		/// <summary>
		/// Gets or sets the API key.
		/// </summary>
		/// <value>The API key.</value>
		string ApiKey { get; set; }

		/// <summary>
		/// Gets or sets the base address.
		/// </summary>
		/// <value>The base address.</value>
		string BaseAddress { get; set; }
	}
}
