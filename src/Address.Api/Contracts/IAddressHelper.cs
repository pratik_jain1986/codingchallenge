﻿// <copyright file="IAddressHelper.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Contracts
{
	using System.Threading.Tasks;
	using Address.Api.Models.Request;
	using Address.Api.Models.Response;

	/// <summary>
	/// Interface IAddressHelper
	/// </summary>
	public interface IAddressHelper
	{
		/// <summary>
		/// Gets the address.
		/// </summary>
		/// <param name="addressRequest">The address request.</param>
		/// <returns>Task&lt;AddressResponse&gt;.</returns>
		Task<AddressResponse> GetAddress(AddressRequest addressRequest);
	}
}
