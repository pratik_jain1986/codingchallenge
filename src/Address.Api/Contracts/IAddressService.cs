﻿// <copyright file="IAddressService.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Contracts
{
	using System.Threading.Tasks;
	using Address.Api.Common.Models.Google;

	/// <summary>
	/// Interface IAddressService
	/// </summary>
	public interface IAddressService
	{
		/// <summary>
		/// Gets the address from google maps api for the given latLong.
		/// </summary>
		/// <param name="latLong">The lat long.</param>
		/// <returns>Task&lt;ResponseAddress&gt;.</returns>
		Task<ResponseAddress> GetAddress(string latLong);
	}
}
