﻿// <copyright file="IAddressRequestValidator.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Contracts
{
	using Address.Api.Models.Request;
	using FluentValidation;

    /// <summary>
    /// Interface IAddressRequestValidator
    /// Implements the <see cref="FluentValidation.IValidator{A2b.Address.Api.Models.Request.AddressRequest}" />
    /// </summary>
    /// <seealso cref="FluentValidation.IValidator{A2b.Address.Api.Models.Request.AddressRequest}" />
    public interface IAddressRequestValidator : IValidator<AddressRequest>
	{
	}
}
