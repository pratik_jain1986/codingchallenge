﻿// <copyright file="ResponseAddressExtensions.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Extensions
{
	using System.Collections.Generic;
	using Address.Api.Common.Models.Google;
	using Models;

	/// <summary>
	/// Class ResponseAddressExtensions.
	/// </summary>
	public static class ResponseAddressExtensions
	{
		/// <summary>
		/// Converts to addresses.
		/// </summary>
		/// <param name="responseAddress">The response address.</param>
		/// <returns>List&lt;Address&gt;.</returns>
		public static List<Address> ToAddresses(this ResponseAddress responseAddress)
		{
			var addresses = new List<Address>();

			foreach (var responseAddressAddressResult in responseAddress.AddressResults)
			{
				addresses.Add(item: new Address
				{
					FormattedAddress = responseAddressAddressResult.FormattedAddress,
					PlaceId = responseAddressAddressResult.PlaceId
				});
			}

			return addresses;
		}
	}
}
