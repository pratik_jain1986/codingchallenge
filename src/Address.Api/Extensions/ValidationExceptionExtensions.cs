﻿// <copyright file="ValidationExceptionExtensions.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Extensions
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using FluentValidation;

	public static class ValidationExceptionExtensions
	{
		public static HttpStatusCode ToStatusCode(
			this ValidationException validationException, Dictionary<string, HttpStatusCode> errorCodesMapping)
		{
			var statusCode = HttpStatusCode.InternalServerError;

			var error = validationException.Errors.First();

			if (errorCodesMapping.ContainsKey(key: error.ErrorCode))
			{
				statusCode = errorCodesMapping[key: error.ErrorCode];
			}

			return statusCode;
		}
	}
}
