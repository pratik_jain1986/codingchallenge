// <copyright file="Startup.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api
{
    using System;
    using Address.Api.Common.Constants;
    using Address.Api.Contracts;
    using Address.Api.Helpers;
    using Address.Api.Models;
    using Address.Api.Services;
    using Address.Api.Validators;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var googleApiConfiguration = new GoogleApiConfiguration
            {
                ApiKey = Configuration["GoogleApiConfiguration:ApiKey"],
                BaseAddress = Configuration["GoogleApiConfiguration:BaseAddress"]
            };

            services.AddSingleton<IGoogleApiConfiguration>(googleApiConfiguration);

            services.AddHttpClient(name: GoogleApi.HttpClientName,
                                                        configureClient: c => { c.BaseAddress = new Uri(uriString: googleApiConfiguration.BaseAddress); });

            services.AddSingleton<IAddressRequestValidator, AddressRequestValidator>();
            services.AddSingleton<IAddressService, AddressService>();
            services.AddSingleton<IAddressHelper, AddressHelper>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Address.Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Address.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
