﻿// <copyright file="MockHttpMessageHandler.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Tests.Services
{
	using System;
	using System.Net;
	using System.Net.Http;
	using System.Threading;
	using System.Threading.Tasks;

	public class MockHttpMessageHandler : HttpMessageHandler
	{
		private readonly bool _isThrowException;
		private readonly string _responseToReturn;
		private readonly HttpStatusCode _responseStatusCode;

		public MockHttpMessageHandler(string responseToReturn, HttpStatusCode responseStatusCode, bool isThrowException = false)
		{
			_isThrowException = isThrowException;
			_responseToReturn = responseToReturn;
			_responseStatusCode = responseStatusCode;
		}

		protected override async Task<HttpResponseMessage> SendAsync(
			HttpRequestMessage request,
			CancellationToken cancellationToken)
		{
			if (_isThrowException)
			{
				throw new Exception();
			}

			var responseMessage = new HttpResponseMessage(_responseStatusCode)
			{
				Content = new StringContent(_responseToReturn)
			};

			return await Task.FromResult(responseMessage);
		}
	}
}
