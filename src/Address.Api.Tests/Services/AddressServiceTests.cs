﻿// <copyright file="AddressServiceTests.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Tests.Services
{
	using System;
	using System.Net;
	using NSubstitute;
	using System.Net.Http;
	using System.Threading.Tasks;
	using Address.Api.Common.Constants;
	using Address.Api.Common.Exceptions;
	using Address.Api.Common.Models.Google;
	using Address.Api.Contracts;
	using Address.Api.Services;
	using AutoFixture.Xunit2;
	using DeepEqual.Syntax;
	using Newtonsoft.Json;
	using NFluent;
	using Xunit;

	public class AddressServiceTests
	{
		[Theory]
		[AutoData]
		public void GetAddress_ThrowsAddressFetchException_WhenFailedToSendRequest(string latLong, string responseToReturn)
		{
			// arrange
			var getMockedHttpClient = new HttpClient(new MockHttpMessageHandler(responseToReturn, HttpStatusCode.OK, true));
			getMockedHttpClient.BaseAddress = new Uri(GoogleApi.BaseAddress);

			var googleApiConfiguration = Substitute.For<IGoogleApiConfiguration>();
			var httpClientFactory = Substitute.For<IHttpClientFactory>();
			httpClientFactory.CreateClient(GoogleApi.HttpClientName).Returns(getMockedHttpClient);

			var addressService = new AddressService(httpClientFactory, googleApiConfiguration);

			// act & assert
			Check.ThatAsyncCode(async () => await addressService.GetAddress(latLong)).Throws<AddressFetchException>();
		}

		[Theory]
		[InlineAutoData(HttpStatusCode.BadRequest)]
		[InlineAutoData(HttpStatusCode.NotAcceptable)]
		[InlineAutoData(HttpStatusCode.Forbidden)]
		[InlineAutoData(HttpStatusCode.BadGateway)]
		public void GetAddress_ThrowsAddressFetchException_WhenReceivedFailedResponseCode(HttpStatusCode responseStatusCode, string latLong, string responseToReturn)
		{
			// arrange

			var getMockedHttpClient = new HttpClient(new MockHttpMessageHandler(responseToReturn, responseStatusCode));
			getMockedHttpClient.BaseAddress = new Uri(GoogleApi.BaseAddress);

			var googleApiConfiguration = Substitute.For<IGoogleApiConfiguration>();
			var httpClientFactory = Substitute.For<IHttpClientFactory>();
			httpClientFactory.CreateClient(GoogleApi.HttpClientName).Returns(getMockedHttpClient);

			var addressService = new AddressService(httpClientFactory, googleApiConfiguration);

			// act & assert
			Check.ThatAsyncCode(async () => await addressService.GetAddress(latLong)).Throws<AddressFetchException>();
		}

		[Theory, AutoData]
		public void GetAddress_ThrowsAddressFetchException_WhenGoogleApiResponseIsNotOk(string latLong, ResponseAddress response)
		{
			// arrange
			var responseToReturn = JsonConvert.SerializeObject(response);

			var getMockedHttpClient = new HttpClient(new MockHttpMessageHandler(responseToReturn, HttpStatusCode.OK));
			getMockedHttpClient.BaseAddress = new Uri(GoogleApi.BaseAddress);

			var googleApiConfiguration = Substitute.For<IGoogleApiConfiguration>();

			var httpClientFactory = Substitute.For<IHttpClientFactory>();
			httpClientFactory.CreateClient(GoogleApi.HttpClientName).Returns(getMockedHttpClient);

			var addressService = new AddressService(httpClientFactory, googleApiConfiguration);

			// act & assert
			Check.ThatAsyncCode(async () => await addressService.GetAddress(latLong)).Throws<AddressFetchException>();
		}

		[Theory, AutoData]
		public async Task GetAddress_ReturnsExpectedResponse_WhenGoogleApiResponseIsNotOk(string latLong, ResponseAddress response)
		{
			// arrange
			response.Status = GoogleApi.StatusOk;

			var responseToReturn = JsonConvert.SerializeObject(response);
			var getMockedHttpClient = new HttpClient(new MockHttpMessageHandler(responseToReturn, HttpStatusCode.OK));
			getMockedHttpClient.BaseAddress = new Uri(GoogleApi.BaseAddress);

			var googleApiConfiguration = Substitute.For<IGoogleApiConfiguration>();

			var httpClientFactory = Substitute.For<IHttpClientFactory>();
			httpClientFactory.CreateClient(GoogleApi.HttpClientName).Returns(getMockedHttpClient);

			var addressService = new AddressService(httpClientFactory, googleApiConfiguration);

			// act
			var actualAddress = await addressService.GetAddress(latLong);

			// assert
			Check.That(actualAddress.IsDeepEqual(response)).IsTrue();
		}
	}
}
