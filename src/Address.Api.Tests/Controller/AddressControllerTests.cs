﻿// <copyright file="AddressControllerTests.cs" company="XXXX">
// Copyright by XXXX PTY LTD. All rights reserved.
// </copyright>

namespace Address.Api.Tests.Controller
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using System.Threading.Tasks;
	using Address.Api.Common.Constants;
	using Address.Api.Common.Exceptions;
	using Address.Api.Contracts;
	using Address.Api.Controllers;
	using Address.Api.Models.Request;
	using Address.Api.Models.Response;
	using AutoFixture.Xunit2;
	using DeepEqual.Syntax;
	using FluentValidation;
	using FluentValidation.Results;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.Logging;
	using NFluent;
	using NSubstitute;
	using NSubstitute.ExceptionExtensions;
	using Xunit;

	public class AddressControllerTests
	{
		[Theory, AutoData]
		public async Task GetAddress_ReturnsBadRequestResponse_WhenValidationExceptionIsThrown(string pickupLocation, string destinationLocation, ValidationFailure error)
		{
			// arrange
			var logger = Substitute.For<ILogger<AddressController>>();
			var addressHelper = Substitute.For<IAddressHelper>();
			error.ErrorCode = ErrorCodes.BadRequest;

			addressHelper.GetAddress(Arg.Any<AddressRequest>()).Throws(new ValidationException(new List<ValidationFailure>
			{
				error
			}));

			var addressController = new AddressController(logger, addressHelper);

			// act
			var response = await addressController.GetAddress(pickupLocation, destinationLocation);

			// assert
			var result = response as ObjectResult;
			Check.That(result.StatusCode).IsEqualTo((int)HttpStatusCode.BadRequest);
		}

		[Theory, AutoData]
		public async Task GetAddress_ReturnsNotAcceptableResponse_WhenFailedToFetchAddress(string pickupLocation, string destinationLocation, string errorMessage, string status)
		{
			// arrange
			var logger = Substitute.For<ILogger<AddressController>>();
			var addressHelper = Substitute.For<IAddressHelper>();
			addressHelper.GetAddress(Arg.Any<AddressRequest>()).Throws(new AddressFetchException(errorMessage, status));

			var addressController = new AddressController(logger, addressHelper);

			// act
			var response = await addressController.GetAddress(pickupLocation, destinationLocation);

			// assert
			var result = response as ObjectResult;
			Check.That(result.StatusCode).IsEqualTo((int)HttpStatusCode.NotAcceptable);
		}

		[Theory, AutoData]
		public async Task GetAddress_ReturnsInternalServerResponse_WhenUnknownErrorOccurs(string pickupLocation, string destinationLocation, string errorMessage)
		{
			// arrange
			var logger = Substitute.For<ILogger<AddressController>>();
			var addressHelper = Substitute.For<IAddressHelper>();
			addressHelper.GetAddress(Arg.Any<AddressRequest>()).Throws(new Exception(errorMessage));

			var addressController = new AddressController(logger, addressHelper);

			// act
			var response = await addressController.GetAddress(pickupLocation, destinationLocation);

			// assert
			var result = response as ObjectResult;
			Check.That(result.StatusCode).IsEqualTo((int)HttpStatusCode.InternalServerError);
		}


		[Theory, AutoData]
		public async Task GetAddress_ReturnsOkResponse_WhenNoError(string pickupLocation, string destinationLocation, AddressResponse expectedReponse)
		{
			// arrange
			var logger = Substitute.For<ILogger<AddressController>>();
			var addressHelper = Substitute.For<IAddressHelper>();
			addressHelper.GetAddress(Arg.Any<AddressRequest>()).Returns(expectedReponse);

			var addressController = new AddressController(logger, addressHelper);

			// act
			var response = await addressController.GetAddress(pickupLocation, destinationLocation);

			// assert
			var actualResult = response as ObjectResult;

			Check.That(actualResult.Value.IsDeepEqual(expectedReponse)).IsTrue();
		}
	}
}
