## Coding challenge

Write an end point that returns address information from geographic coordinates (latitude and longitude).

An endpoint takes 2 sets (e.g. a Pickup location and a Destination location) of
geographic coordinates (latitude and longitude), calling Google Maps
Geocoding API to return a response containing:

- Google Formatted_address of each geographic coordinate
- Google Place id of each geographic coordinate

#### Solution implementation

###### Input is validated before calling maps api, in this way we can avoid invalid requests being made to maps api which saves cost.

- 100% code coverage for address service and controller
- Developed using SOLID design principles
- Classes are loosely coupled
- Currently api returns list of matched locations.(We could return the top first formatted address from maps api)

###### Main classes and responsibilities

- _AddressRequestValidator.cs_ - Validates input for correct lat long values
- _AddressService.cs_ - Service to communicate with google maps api

#### Steps to run and test

Open the solution src\Address.Api\Address.Api.sln in visual studio. Build and run(or debug).
It opens swagger page at location http://localhost:14787/swagger/index.html.

Click on 'Try it out' against endpoint 'GET /Address'.
Enter values for 'pickupLocationLatLong', 'destinationLocationLatLong' and click execute to see result

##### Example 1 – Valid request

###### Request URL : ~/Address?pickupLocationLatLong=-33.85663386202557%2C%20151.21523355464714&destinationLocationLatLong=-33.87414139134592%2C%20151.20081888712227

###### Response (200 Ok response)

    {
    "pickupLocations": [
    	{
    		"formattedAddress": "2 Circular Quay E, Sydney NSW 2000, Australia",
    		"placeId": "ChIJu8tR9WauEmsRt-zRIuB7kMs"
    	},
    	{
    		"formattedAddress": "Unnamed Road, Sydney NSW 2000, Australia",
    		"placeId": "ChIJjT98mWauEmsRtHFY7qDXNHs"
    	},
    	..........................
    ],
    "destinationLocations": [
    	{
    		"formattedAddress": "A4, Sydney NSW 2000, Australia",
    		"placeId": "ChIJRT2K9zquEmsRmKCGtbjE5hg"
    	},
    	{
    		"formattedAddress": "Sydney NSW 2000, Australia",
    		"placeId": "ChIJP5iLHkCuEmsRwMwyFmh9AQU"
    	},
    	............................
    ]

}

##### Example 2 – Invalid input

###### Request URL : /Address?pickupLocationLatLong=232%2C232&destinationLocationLatLong=adf%2Csfsd

###### Response (400 Bad Request)

    {
        "errorCode": "address-002",
        "errorMessage": "Invalid lat long value"
    }

##### Future enhancements

Create separate project for services and make it nuget pkg and use it in multiple projects
Handle rate limiting errors from maps api
